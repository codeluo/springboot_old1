package com.spr.test.springboot;

import java.nio.IntBuffer;

public class NioTest {

    public static void main(String[] args) {
        IntBuffer buffer = IntBuffer.allocate(10);
        buffer.put(1);
        buffer.put(2);
        buffer.put(3);
//        buffer.flip();
        buffer.put(4);
        buffer.flip();
        for(int i = 0;i<buffer.limit();i++){
            System.out.println(buffer.get(i));
        }

    }
}
