package com.spr.test.springboot.controller;

import com.spr.test.springboot.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value = "/redis")
public class RedisController {

    @Autowired
    private UserInfoService userService ;

    @RequestMapping("/insert")
    @ResponseBody
    public String insert(HttpServletRequest request , HttpServletResponse response , Model model){
        String key = request.getParameter("key");
        String value = request.getParameter("value");
        return "success";
    }
}
