package com.spr.test.springboot.controller;

import com.spr.test.springboot.common.BaseController;
import com.spr.test.springboot.common.Global;
import com.spr.test.springboot.service.UserOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "/userOption")
public class UserOptionController extends BaseController{

    @Autowired
    private UserOptionService userOptionService ;

    @PostMapping(value = "/addOption")
    public String addOption(@RequestParam Map<String, String> params,String optionName){
//        UserOption userOption = new UserOption();
//        userOption.setOptionName(optionName);
//        userOption.setOptionValue(JsonMapper.toJsonString(params));
//        userOptionService.addOption(userOption);

        String option = Global.getOption("system_set", "sys_name");

        return success("成功",null);

    }
}
