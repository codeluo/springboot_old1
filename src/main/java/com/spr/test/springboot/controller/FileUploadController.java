package com.spr.test.springboot.controller;

import com.spr.test.springboot.common.BaseController;
import com.spr.test.springboot.dao.PhotoDao;
import com.spr.test.springboot.entity.Photo;
import com.spr.test.springboot.execption.MyExecption;
import com.spr.test.springboot.service.UploadFileService;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/photo")
public class FileUploadController extends BaseController{

    @Resource
    private UploadFileService fileService ;
    @Resource
    private PhotoDao photoDao ;

    @PostMapping(value = "getPhotoList")
    public String getPhotoList(Model model){
        List<Photo> list = photoDao.findAll();
        model.addAttribute("photos",list);
        return success("成功",model);
    }

    @PostMapping(value = "addIndexPhoto")
    public String addIndexPhoto(Photo photo, @RequestParam("file") MultipartFile file) {
        String description = photo.getDescription();
        try {
            String path = fileService.uploadFile(file);
            photo.setUrl(path);
            photo.setCreateDate(new Date());
            photoDao.save(photo);
        } catch (MyExecption e) {
            return fail("图片上传："+e.getMessage());
        }
        return "上传成功";
    }

    @RequestMapping(value = "upload")
    public String upload(HttpServletRequest request ,@RequestParam("file") MultipartFile file) {

        try {
            fileService.uploadFile(file);
        } catch (MyExecption e) {
            return fail("上传失败");
        }

        return "上传成功";
    }



    @RequestMapping(value = "uploadMore")
    public String upload(HttpServletRequest request ,@RequestParam(value = "file", required = false)List<MultipartFile> files) {
        if (files.isEmpty()) {
            return "文件为空";
        }

        for (MultipartFile file :files) {

            try {
                fileService.uploadFile(file);
            } catch (MyExecption e) {
                e.printStackTrace();
            }
        }

        return fail("上传失败");
    }


    /**
     * 文件类型转换
     */
    public File multipartToFile(MultipartFile multfile) {

        CommonsMultipartFile cf = (CommonsMultipartFile) multfile;
        //这个myfile是MultipartFile的
        DiskFileItem fi = (DiskFileItem) cf.getFileItem();
        File file = fi.getStoreLocation();
        return file;
    }
}
