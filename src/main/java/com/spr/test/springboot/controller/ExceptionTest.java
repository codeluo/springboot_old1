package com.spr.test.springboot.controller;

import com.spr.test.springboot.execption.MyExecption;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/exception")
public class ExceptionTest {

    /**
     * 两个全局捕获异常测试
     *
     * @return
     * @throws MyExecption
     */
    @RequestMapping(value = "/runTimeException")
    public String index(){

        int a = 1 / 0;

        return "index/index";
    }

    /**
     * 两个全局捕获异常测试
     *
     * @return
     * @throws MyExecption
     */
    @RequestMapping(value = "/myException")
    public String myException() throws MyExecption {

        try {
            int a = 1 / 0;
        } catch (Exception e) {
            throw new MyExecption("系统异常,请稍后再试");
        }

        return "index/index";
    }
}
