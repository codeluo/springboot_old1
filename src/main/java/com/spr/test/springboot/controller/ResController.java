package com.spr.test.springboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/rest")
public class ResController {

    @RequestMapping(value = "/data",method = RequestMethod.GET)
    public Map<String,List<String>> getData(){
        Map<String,List<String>> map = new HashMap<>() ;
        List<String> list = new ArrayList<>() ;
        list.add("one");
        list.add("two");
        map.put("one",list);
        map.put("two",list);
        return map;
    }
}
