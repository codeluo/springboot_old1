package com.spr.test.springboot.execption;

public class MyExecption extends Exception {

    public MyExecption() {
    }

    public MyExecption(String message) {
        super(message);
    }
}
