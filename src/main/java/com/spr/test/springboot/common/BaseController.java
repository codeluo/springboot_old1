package com.spr.test.springboot.common;

import com.spr.test.springboot.Util.JsonMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class BaseController {

    public String fail(String message){
        Map map = new HashMap() ;
        map.put("status",0);
        map.put("message",message);
        return JsonMapper.toJsonString(map);
    }

    public String success(String message, Model model){
        if(model == null){
            return JsonMapper.toJsonString(message);
        }
        model.addAttribute("message",message);
        model.addAttribute("status",1);
        return JsonMapper.toJsonString(model);
    }
}
