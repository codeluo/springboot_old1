package com.spr.test.springboot.common;

import com.spr.test.springboot.Util.JsonMapper;
import com.spr.test.springboot.entity.UserOption;
import com.spr.test.springboot.service.UserOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class Global {

    @Autowired
    private  UserOptionService userOptionService ;

    private static Global global;

    @PostConstruct
    public void init(){
        global = this;
        global.userOptionService = this.userOptionService;
    }

    public static String getOption(String optionName, String lableName){
                UserOption userOption = global.userOptionService.getByOptionName(optionName);
        if(userOption != null){
            Map<String, String> map = (Map<String, String>) JsonMapper.fromJsonString(userOption.getOptionValue(), HashMap.class);
            return map.get(lableName);
        }
        return "";




    }
}
