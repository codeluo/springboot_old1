package com.spr.test.springboot.activemq;


import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

@Component
public class Consumer2 {

    @JmsListener(destination = "mytest.queue")
    @SendTo("activemq")
    public String receiveQueue(String text) {


        System.out.println("罗杰收到消息为:"+text);
        return "罗杰已经处理完成-->"+text;
    }
}
