package com.spr.test.springboot.Util;


public class RandomUtil {

    public static int getRandom(int temp) {
        return (int) (Math.random() * temp);
    }

    public static String getRandomString(int temp) {
        return String.valueOf((int)(Math.random() * temp));
    }

}
