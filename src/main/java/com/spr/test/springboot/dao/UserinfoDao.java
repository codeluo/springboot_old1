package com.spr.test.springboot.dao;

import com.spr.test.springboot.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserinfoDao extends JpaRepository<UserInfo, Integer> {

    UserInfo findByUsername(String userName);
}
