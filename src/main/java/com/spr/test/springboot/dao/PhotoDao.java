package com.spr.test.springboot.dao;

import com.spr.test.springboot.entity.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhotoDao extends JpaRepository<Photo,Integer> {

}
