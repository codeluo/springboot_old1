package com.spr.test.springboot.dao;

import com.spr.test.springboot.entity.UserOption;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserOptionDao extends JpaRepository<UserOption,Integer> {

    public UserOption getByOptionName(String optionName);
}
