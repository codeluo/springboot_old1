package com.spr.test.springboot.systemTool;

import com.spr.test.springboot.execption.MyExecption;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExecptionHandler {

    @ExceptionHandler(value = RuntimeException.class)
    public String gloablExecption(HttpServletRequest request , HttpServletResponse response,Exception e,Model model){

        model.addAttribute("url",request.getRequestURI());
        model.addAttribute("error",e);
        return "error/error";
    }

    @ExceptionHandler(value = MyExecption.class)
    @ResponseBody
    public Map<String,Object> gloablMyExecption(HttpServletRequest request , HttpServletResponse response, Exception e, Model model){
        Map<String,Object> map = new HashMap<>() ;
        map.put("error",e.getMessage());
        return map;
    }
}

