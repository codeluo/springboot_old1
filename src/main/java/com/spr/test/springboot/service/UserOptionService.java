package com.spr.test.springboot.service;

import com.spr.test.springboot.common.Global;
import com.spr.test.springboot.dao.UserOptionDao;
import com.spr.test.springboot.entity.UserOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;


@Service
public class UserOptionService {

    @Autowired
    private UserOptionDao userOptionDao ;


    public void addOption(UserOption userOption ){

        userOptionDao.save(userOption);
    }

    public UserOption getByOptionName(String optionName){

        return userOptionDao.getByOptionName(optionName);
    }
}
