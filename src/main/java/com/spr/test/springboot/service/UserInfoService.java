package com.spr.test.springboot.service;

import com.spr.test.springboot.dao.UserinfoDao;
import com.spr.test.springboot.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoService {

    @Autowired
    private UserinfoDao userinfoDao ;

    public UserInfo findByUsername(String userName) {
        return userinfoDao.findByUsername(userName);
    }
}
